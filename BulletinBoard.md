# CSE 262 - Bulletin Board

## :computer: Homework

- [ ] 12/03 - [Homework 10](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-10) - Perspectives on Programming Langauges
- [x] 11/22 - [Homework 9](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-9) - Prolog - Checkers
- [x] 11/08 - [Homework 8](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-8) - Haskell - Functional Problem Solving
- [x] ~~10/18~~ 10/25 - [Homework 7b](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-7b) - Rust IV - A Parser Part II - [Solution](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-7b/-/tree/solutions)
- [x] ~~10/18~~ 10/25 - [Homework 7a](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-7a) - Rust IV - A Parser Part I - [Solution](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-7a/-/tree/solutions)
- [x] 10/04 - [Homework 6](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-6) - Rust III - Linked List - [Solution](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-6/-/tree/solutions)
- [x] 09/27 - [Homework 5](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-5) - Rust II - A Lexer - [Solution](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-5/-/tree/solutions)
- [x] 09/20 - [Homework 4](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-4) - Rust I - Rust Fundamentals
- [x] 09/13 - [Homework 3](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-3) - Python II - Fun with Python and Spotify
- [x] 09/06 - [Homework 2](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-2) - Python I - Java to Python Translation
- [x] 08/30 - [Homework 1](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/homework-1) - Assignment submission process
- [x] 08/30 - [Homework 0](https://gitlab.com/lehigh-cse-262/fall-2021/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab
- [ ] 12/03 - [Participation](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/participation)

## :checkered_flag: Quizzes

- [ ] 12/03 - [Quiz 11](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-11) (Extra Credit)
- [x] 11/26 - [Quiz 10](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-10)
- [x] 11/09 - [Quiz 9](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-9)
- [x] 10/29 - [Quiz 8](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-8)
- [x] 10/22 - [Quiz 7](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-7)
- [x] 10/08 - [Quiz 6](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-6)
- [x] 10/01 - [Quiz 5](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-5)
- [x] 09/24 - [Quiz 4](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-4)
- [x] 09/17 - [Quiz 3](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-3)
- [x] 09/10 - [Quiz 2](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-2)
- [x] 09/03 - [Quiz 1](https://gitlab.com/lehigh-cse-262/fall-2021/assignments/quiz-1)

## :books: Readings

| Readings           |
| ------------------ |
|**Week 14**|
| <ul><li>[The Future of Programming](http://worrydream.com/dbx/) - Bret Victor</li><li>[A human view of programming languages](https://medium.com/bits-and-behavior/my-splash-2016-keynote-81cc802f5f6e) - Amy J. Ko</li><li>[Dynamic Land](https://dynamicland.org)</li><li>[No Silver Bullet](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf) - Fred Brooks</li><li>[Out of the Tar Pit](http://curtclifton.net/papers/MoseleyMarks06a.pdf) - Ben Mosely and Peter Marks</li></ul>|
|**Week 13**|
| <ul><li>[Logic, Programming, and Prolog](https://www.ida.liu.se/~ulfni53/lpp/bok/bok.pdf) - Chapters 2, 3, 4</li></ul>|
|**Week 12**|
| <ul><li>[Logic, Programming, and Prolog](https://www.ida.liu.se/~ulfni53/lpp/bok/bok.pdf) - Chapters 1, 6, and 7</li><li>[SWI Prolog Tutorial](https://www.swi-prolog.org/pldoc/man?section=quickstart)</li><li>[SWI Prolog Manual](https://www.swi-prolog.org/pldoc/doc_for?object=manual)</li></ul>|
|**Week 11**|
| <ul><li>[Learn You a Haskell](http://learnyouahaskell.com/chapters) - Chapters 10 - 12</li></ul>|
|**Week 10**|
| <ul><li>[Learn You a Haskell](http://learnyouahaskell.com/chapters) - Chapters 4 - 6</li></ul>|
|**Week 9**|
| <ul><li>[Learn You a Haskell](http://learnyouahaskell.com/chapters) - Chapters 1 - 3</li><li>[Haskell GHC Download](https://www.haskell.org/downloads/)</li><li>[Haskell Documentation](https://www.haskell.org/documentation/)</li></ul>|
|**Week 8**|
| <ul><li>[Nom Parser Combinator Library for Rust](https://docs.rs/nom/7.0.0/nom/)</li><li>[List of parsers and combinators](https://github.com/Geal/nom/blob/master/doc/choosing_a_combinator.md)</li></ul>|
|**Week 7**|
| <ul><li>[Crafting Interpreters](https://craftinginterpreters.com/parsing-expressions.html) - Chapter 6 if you didn't read it last week.</li><li>[Nom Parser Combinator Library Docs](https://docs.rs/nom/7.0.0/nom/index.html)</li></ul>|
|**Week 6**|
| <ul><li>[The Rust Book](https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html) - Chapter 15</li><li>[std::rc](https://doc.rust-lang.org/std/rc/index.html)</li><li>[std::cell](https://doc.rust-lang.org/std/cell/)</li><li>[std::boxed](https://doc.rust-lang.org/std/boxed/index.html)</li><li>[Crafting Interpreters](https://craftinginterpreters.com/parsing-expressions.html) - Chapter 6. This will probably be more important next week, but you can start reading it now.</li></ul>|
|**Week 5**|
| <ul><li>Continue Reading Rust Book Chapters 3 - 10</li><li>[Crafting Interpreters](https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html) - Chapters 1-4</li></ul>|
|**Week 4**|
| <ul><li>[The Rust Book](https://doc.rust-lang.org/stable/book/ch03-00-common-programming-concepts.html) - Chapters 3 - 10. The way I would approach this reading is to do it side-by-side with the homework. Go to the associated section in the book and use it to help you solve the problems. It's not important if you don't read every word in all these chapters. Read enough to help you pass the HW tests.</li></ul>|
|**Week 3**|
| <ul><li>[A Byte of Python](https://python.swaroopch.com/) - Modules through Standard Library</li><li>[Python Data Science Tutorial](https://www.tutorialspoint.com/python_data_science/index.htm)</li><li>[Matplotlib Tutorials](https://matplotlib.org/stable/tutorials/index.html)</li></ul>|
|**Week 2**|
|<ul><li>[A Byte of Python](https://python.swaroopch.com/) - Introduction through Functions</li><li>[Differences Between Python 2 and 3](https://www.thecrazyprogrammer.com/2018/01/difference-python-2-3.html)</li><li>[The Zen of Python](https://www.python.org/dev/peps/pep-0020/)</li><ul>
|**Week 1**|
|[The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6

## :vhs: Lectures
- [FA21 Recordings](https://drive.google.com/drive/folders/1-HEz-t5_-ITRr54d166AAsK5qj_cRdLW?usp=sharing)
- [FA20 Playlist](https://www.youtube.com/playlist?list=PL4A2v89SXU3Trn3mBoocRdPwDULMyk_id)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|Lecture 15 | 10/13 | Rust IX | [Video](https://drive.google.com/file/d/1Y4UHFuT63PNy1llSnLmaNODY5UchCt_Z/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/ConventionalAgedCompiler)
|Lecture 14 | 10/06 | Rust VIII | [Video](https://drive.google.com/file/d/1OX9xpeZ1ftT7uS_vtgGFhZWkuaiarwxc/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/AdoredOpulentRobot)
|Lecture 13 | 10/04 | Rust VII | [Video](https://drive.google.com/file/d/1Xpvb6ftB27mJtKhhR_x6eRAh7enhjNdK/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/ColossalNeatQuote#src/main.rs)
|Lecture 12 | 09/29 | Rust VI | [Video](https://drive.google.com/file/d/1a5YKueYm778gKX0nkgIL7bTHn1gag4l2/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/BitesizedRectangularPort#src/main.rs)
|Lecture 11 | 09/27 | Rust V| [Video](https://drive.google.com/file/d/1MUGqGiXAQcXtO4G6RS1dpoUKuhLmRqSg/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/GoodnaturedBisqueExpertise)
|Lecture 10 | 09/22 | Rust IV| [Video](https://drive.google.com/file/d/1bqJCkM5tkoRJ-B-fCz3PSaMlLgekfwpE/view?usp=sharing)
|Lecture 09 | 09/20 | Rust III | [Video](https://drive.google.com/file/d/18AWY5PfhqWYpWu2m9QRuMGcu2KxkUedt/view?usp=sharing) - [Code](https://replit.com/@CoreyMontella/VioletTautBackground#src/main.rs)
|Lecture 08 | 09/15 | Rust II | [Video](https://drive.google.com/file/d/1QWHzR7YwWlKX394qUU_9_zYf1kjQv9na/view?usp=sharing)
|Lecture 07 | 09/13 | Rust I | [Video](https://drive.google.com/file/d/10g56Lh1TaaMttgeKYpwx50NOfo7PFfoS/view?usp=sharing)
|Lecture 06 | 09/08 | Python IV | [Video](https://drive.google.com/file/d/167XqFJb4vZOvy5Fo77fOHujeH-s_HM8i/view?usp=sharing)
|Lecture 05 | 09/06 | Python III | [Video](https://drive.google.com/file/d/1JJUGk2eOxuKVwS0-u0XysMeFEvrdcbk0/view?usp=sharing)
|Lecture 04 | 09/01 | Python II | [Video](https://drive.google.com/file/d/1ZfAbDGFMeqn5vKDykc-TfWtKBCufAXHi/view?usp=sharing)
|Lecture 03 | 08/30 | Python I | [Video](https://drive.google.com/file/d/1Z7P5oyqA9aqQth0e5yvixQLOJCC0GywV/view?usp=sharing)
|Lecture 02 | 08/25 | Git and Gitlab Fundamentals | [Video](https://drive.google.com/file/d/1-9dQwGxY6HrPkvGJUr9JbtTJb98wj1xk/view?usp=sharing)
|Lecture 01 | 08/23 | Course Introduction | [Video](https://drive.google.com/file/d/1IOioqiKDanjnlOhy4YM3O6MtxdxPerQv/view?usp=sharing)

## 📅 Course Calendar

Below is a tentative schedule for the semester, broken down by week. Topics covered and the order in which they are covered is subjet to change. Any changes will be reflected here. It is your responsibility to stay up to date with this calendar over the course of this semester.


| Week                      | Monday        | Wednesday          | Friday          |
| ------------------------- | --------------| ------------------ | --------------- |
| Week 1 (Aug 23 - Aug 27)  | [Syllabus day](https://drive.google.com/file/d/1IOioqiKDanjnlOhy4YM3O6MtxdxPerQv/view?usp=sharing)  | [Intro to Git and Gitlab](https://drive.google.com/file/d/1-9dQwGxY6HrPkvGJUr9JbtTJb98wj1xk/view?usp=sharing) | [Recitation 1](https://drive.google.com/file/d/1-5K8OE8maSM-uSGODFN_ZwoT0IKJx1rn/view?usp=sharing)    |
| Week 2 (Aug 30 - Sep 03)  | [Python I](https://drive.google.com/file/d/1Z7P5oyqA9aqQth0e5yvixQLOJCC0GywV/view?usp=sharing) - Intro to Python      | [Python II](https://drive.google.com/file/d/1ZfAbDGFMeqn5vKDykc-TfWtKBCufAXHi/view?usp=sharing) - Python vs. Java         | [Recitation 2](https://drive.google.com/file/d/1DbtWNHFowrgeqL-LUrv8t0EkX2LldZMn/view?usp=sharing)    |
| Week 3 (Sep 06 - Sep 10)  | [Python III](https://drive.google.com/file/d/1JJUGk2eOxuKVwS0-u0XysMeFEvrdcbk0/view?usp=sharing) - Design and Implementation    | [Python IV](https://drive.google.com/file/d/167XqFJb4vZOvy5Fo77fOHujeH-s_HM8i/view?usp=sharing) - Ecosystem          | [Recitation 3](https://drive.google.com/file/d/1OKVG6SH-MnuJ_h3sUoCXYiFykVHUC77v/view?usp=sharing)    |
| Week 4 (Sep 13 - Sep 17)  | [Rust I](https://drive.google.com/file/d/10g56Lh1TaaMttgeKYpwx50NOfo7PFfoS/view?usp=sharing)  - Intro to Rust | [Rust II](https://drive.google.com/file/d/1QWHzR7YwWlKX394qUU_9_zYf1kjQv9na/view?usp=sharing)            | [Recitation 4](https://drive.google.com/file/d/1LhcXRuiMxYVpR8wKXB0LtDWtA5uaLAo5/view?usp=sharing)    |
| Week 5 (Sep 20 - Sep 24)  | [Rust III](https://drive.google.com/file/d/18AWY5PfhqWYpWu2m9QRuMGcu2KxkUedt/view?usp=sharing)      | [Rust IV](https://drive.google.com/file/d/1bqJCkM5tkoRJ-B-fCz3PSaMlLgekfwpE/view?usp=sharing)            | [Recitation 5](https://drive.google.com/file/d/1OwNm_7RcblUWS9MiTeFKK236cSHunK6S/view?usp=sharing)    |
| Week 6 (Sep 27 - Oct 01)  | [Rust V](https://drive.google.com/file/d/1MUGqGiXAQcXtO4G6RS1dpoUKuhLmRqSg/view?usp=sharing)        | [Rust VI](https://drive.google.com/file/d/1a5YKueYm778gKX0nkgIL7bTHn1gag4l2/view?usp=sharing)            | [Recitation 6](https://drive.google.com/file/d/1AMQor4tcbakWzhTm372fWOZo_E-lwNj6/view?usp=sharing)    |
| Week 7 (Oct 04 - Oct 08)  | [Rust VII](https://drive.google.com/file/d/1Xpvb6ftB27mJtKhhR_x6eRAh7enhjNdK/view?usp=sharing)      | [Rust VIII](https://drive.google.com/file/d/1OX9xpeZ1ftT7uS_vtgGFhZWkuaiarwxc/view?usp=sharing)          | [Recitation 7](https://drive.google.com/file/d/1kQ939nKmlpWJroglp3zk7aAF4BfRch61/view?usp=sharing)    |
| Week 8 (Oct 11 - Oct 15)  | Pacing Break (No Class) | [Rust IX](https://drive.google.com/file/d/1Y4UHFuT63PNy1llSnLmaNODY5UchCt_Z/view?usp=sharing)  | [Recitation 8](https://drive.google.com/file/d/10rUI9v35VbEIQgW14_z2Gf1U65OTeLv9/view?usp=sharing)    |
| Week 9 (Oct 18 - Oct 22)  | Haskell I     | Haskell II         | [Recitation 9](https://drive.google.com/file/d/1D0R5x9CYrcoL1ZGEbt5iDUWdhUYSkwVF/view?usp=sharing)    |
| Week 10 (Oct 25 - Oct 29) | Haskell III     | Haskell IV       | [Recitation 10](https://drive.google.com/file/d/1CBVmyUmzM1gDjpGWDMHIGD5-v2iX9HZO/view?usp=sharing)    |
| Week 11 (Nov 01 - Nov 05) | Haskell V     | Haskell VI         | [Recitation 11](https://drive.google.com/file/d/1nOaop0ekS-xurF5w6G3SMJGUz-XxC0hk/view?usp=sharing)*    |
| Week 12 (Nov 08 - Nov 12) | [Prolog I](https://drive.google.com/file/d/1GpnUcTa9hTzImLmjQ5YqjCsUuAUvcGcZ/view?usp=sharing) - Intro to Prolog      | Prolog II          | Recitation 12    |
| Week 13 (Nov 15 - Nov 19) | Prolog III    | Prolog IV          | Recitation 13    |
| Week 14 (Nov 22 - Nov 26) | Future of Programming | Thanksgiving Break (No Class) | Thanksgiving Break (No Class) |
| Week 15 (Nov 29 - Dec 03) | Future of Programming | Future of Programming      | Final Review    |

For your reference: [Lehigh University Academic Calendar](https://ras.lehigh.edu/content/current-students/academic-calendar)

Important dates:

- First day of class: August 23
- *Last day to withdraw with a "W": November 5
- Last day of class: December 3

